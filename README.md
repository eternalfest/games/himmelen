# Himmelen

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/himmelen.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/himmelen.git
```
