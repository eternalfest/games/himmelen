package purgatoire.actions;

import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class Spike implements IAction {
    public var name(default, null): String = Obfu.raw("spike");
    public var isVerbose(default, null): Bool = false;

    private var mod: Purgatoire;

    public function new(mod: Purgatoire) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var x: Float = ctx.getFloat(Obfu.raw("x"));
        var p: Bool = ctx.getBool(Obfu.raw("p"));

        this.mod.addSpike(game, x, p);

        return false;
    }
}
