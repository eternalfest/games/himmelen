package purgatoire;

import purgatoire.actions.Spike;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;


@:build(patchman.Build.di())
class Purgatoire {
    @:diExport
    public var action(default, null): IAction;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new(): Void {
        this.action = new Spike(this);

        var patches = [
            Ref.auto(hf.entity.Bad.update).after(function(hf: hf.Hf, self: hf.entity.Bad): Void {
                if ((cast self).customSpike) {
                    self.fallFactor *= 1.02;
                }
            }),
            Ref.auto(hf.entity.Bad.onDeathLine).wrap(function(hf: hf.Hf, self: hf.entity.Bad, old): Void {
                if ((cast self).customSpike) {
                    self.destroy();
                }
                else {
                    old(self);
                }
            }),
        ];
        this.patches = FrozenArray.from(patches);

    }

    public function addSpike(game: hf.mode.GameMode, x: Float, p: Bool) {
        var spike = game.attachBad(13, game.root.Entity.x_ctr(game.flipCoordCase(x)) - game.root.Data.CASE_WIDTH * 0.5, -40);
        spike.fl_physics = true;
        spike.fl_hitGround = p;
        spike.fl_moveable = false;
        spike.fallFactor = 1;
        spike.gotoAndStop('3');
        (cast spike).customSpike = true;
    }
}
