import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import better_script.Players;
import atlas.Atlas;
import purgatoire.Purgatoire;
import better_script.Rand;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    rand: Rand,
	gameParams: GameParams,
	noNextLevel: NoNextLevel,
    atlasDarkness: atlas.props.Darkness,
    atlasBoss: atlas.props.Boss,
    atlas: atlas.Atlas,
    players: Players,
    purgatoire: Purgatoire,
	merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
